# Summary
The Docker App Server role sets up the latest Docker version for the server

This role is used to setup the following:

* Installs Docker to the machine
* Sets up a default folder for docker data
* Sets up docker socket for management with portainer

# Variables
| Variable name           | Description                                                    | Variable File stored |
|-------------------------|----------------------------------------------------------------|----------------------|
| docker_management_port  | Docker management port. Default is 2375                        | global - application |
| green_ip_address        | Internal IP address binding the MySQL server                   | host                 |

# Dependent projects
| Project Name            |                                                                                           |
| app-server-portainer    | The project installs the portainer agent as a means to manage Docker containers via a GUI |